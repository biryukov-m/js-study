import { handleValidatePhone } from './task_1.js';
import { handleValidateEmail } from './task_1.js';
import { handleValidateName } from './task_1.js';
import { handleCheckAtm } from './task_2.js';

document.querySelector("#validatePhoneClick").addEventListener("click", handleValidatePhone);
document.querySelector("#validateEmailClick").addEventListener("click", handleValidateEmail);
document.querySelector("#validateNameClick").addEventListener("click", handleValidateName);
document.querySelector("#checkAtmClick").addEventListener("click", handleCheckAtm);
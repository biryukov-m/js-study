// - Реалізуємо функції валідації:
// - телефон;
// - email;
// - ПІБ.

// Усі міжгородні/мобільні та інші коди телефонного плану нумерації України
const cityCodes = [31, 33, 34, 35, 36, 37, 38, 41, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 61,
    62, 63, 64, 65, 66, 67, 68, 69, 70, 73, 80, 89, 90, 91, 92, 94, 95, 96, 97, 98, 99];

const validatePhone = text => {
    if (text[0] === '+') {
        text = text.slice(1);
    }
    if (isNaN(text)) {
        return false;
    }
    if (text.slice(0, 3) != '380') {
        return false;
    }
    if (!(cityCodes.includes(+text.slice(3, 5)))) {
        return false;
    }
    if (text.length != 12) {
        return false;
    }
    return true;
}


export const handleValidatePhone = () => {
    const tel = document.querySelector("#validatePhoneInput").value;
    const output = document.querySelector("#validatePhoneResult");
    output.innerHTML = validatePhone(tel) ? 'перевірку пройдено' : 'перевірку не пройдено';
}

const validateEmail = text => /[a-zA-Z0-9]{1,}@[a-zA-Z0-9]{1,}\.[a-zA-Z0-9]{2,3}/.test(text);

export const handleValidateEmail = () => {
    const input = document.querySelector("#validateEmailInput").value;
    const output = document.querySelector("#validateEmailResult");
    output.innerHTML = validateEmail(input) ? 'перевірку пройдено' : 'перевірку не пройдено';
}

const validateName = text => /([A-Za-z]{2,}[ |-]){2,}[A-Za-z]{2,}$/.test(text);

export const handleValidateName = () => {
    const input = document.querySelector("#validateNameInput").value;
    const output = document.querySelector("#validateNameResult");
    output.innerHTML = validateName(input) ? 'перевірку пройдено' : 'перевірку не пройдено';
}
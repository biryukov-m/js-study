// Реалізуємо функцію підрахунку грошей, що видаються
// банкоматом.Користувач запитує суму, а банкомат має
// підібрати варіант із найбільшими купюрами.Врахувати, що
// кількість банкнот обмежена.

const Atm = [
    [1000, 10],
    [500, 20],
    [200, 30],
    [100, 40],
    [50, 50]
]
const checkAtm = amount => {
    if (isNaN(amount) || amount <= 0) {
        return null;
    }
    let outCash = [];
    const atmCopy = [...Atm];
    for (const noteSection of atmCopy) {
        const note = noteSection[0];
        const count = noteSection[1];
        let noteSum = note * count;
        while (amount - note >= 0 && noteSum > 0) {
            amount -= note;
            noteSum -= note;
            outCash.push(note);
        }
    }
    if (amount > 0) {
        return null;
    }
    return outCash;
}


export const handleCheckAtm = () => {
    const amount = document.querySelector("#checkAtmInput").value;
    const output = document.querySelector("#checkAtmResult");
    const result = checkAtm(amount);
    output.innerHTML = result ? `Банкноти:<br> ${result}` : 'Неможливо видати таку суму';
}
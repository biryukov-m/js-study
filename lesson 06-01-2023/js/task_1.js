export const handleFindMax = () => {
    let max;
    for (let i = 1; i <= 10; i++) {
        const value = +document.querySelector(`#findMax${i}Input`).value;
        i == 1 && (max = value);
        value > max && (max = value);
    }
    const output = document.querySelector("#findMaxResult");
    output.innerHTML = max;
}

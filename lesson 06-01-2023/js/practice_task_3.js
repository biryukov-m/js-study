// 3) Вивести всі парні числа із зазначеного діапазону

const getOddFromRange = (start, end) => {
    let result = '';
    for (let i = start; i <= end; i++) {
        if (i % 2 === 0) {
            result += i + ' ';
        }
    }
    return result;
}

export const handleGetOddFromRange = () => {
    const
        start = document.getElementById("getOddFromRangeStart").value,
        end = document.getElementById("getOddFromRangeEnd").value,
        result = getOddFromRange(start, end);
    document.getElementById("getOddFromRangeResult").innerHTML = result;
};
// 4) Користувач вводить номер місяця, потрібно вивести пору року(літо, осінь,
//     зима, весна)

const getSeason = (month) => {
    switch (month) {
        case 12:
        case 1:
        case 2:
            return "Зима";
        case 3:
        case 4:
        case 5:
            return "Весна";
        case 6:
        case 7:
        case 8:
            return "Літо";
        case 9:
        case 10:
        case 11:
            return "Осінь";
    }
    return "Спробуйте ввести місяць з Григоріанського календарю";

}

export const handleGetSeason = () => {
    const input = Number(document.getElementById("getSeasonInput").value);
    console.log(input);
    console.log(typeof input);
    document.getElementById("getSeasonResult").innerHTML = getSeason(input);
}
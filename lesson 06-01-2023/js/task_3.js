const drawPythagorasTable = () => {
    let result = '';
    for (let i = 1; i <= 10; i++) {
        result += '<tr>';
        result += `<th>${i}</th>`;
        for (let j = 1; j <= 10; j++) {
            result += `<td>${i * j}</td>`;
        }
        result += '</tr>';
    }
    return result;
}

export const handleDrawPythagorasTable = () => {
    document.getElementById("pythogorasTable").classList.remove("hide");
    const output = document.getElementById("pythagorasTableOutput");
    const result = drawPythagorasTable();
    output.innerHTML = result;
};
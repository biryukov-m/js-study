const calcucateRunningNorm = (m, k) => {
    if (m <= 0 || k <= 0) {
        return null;
    }
    let day = 0;
    let currentDistanceNorm = Number(m);
    let coeff = Number(k) / 100;
    while (currentDistanceNorm < 50) {
        day++;
        currentDistanceNorm *= (1 + coeff);
    }
    return day;
};

export const handleRunningNorm = () => {
    const m = document.querySelector("#runningNormMInput").value;
    const k = document.querySelector("#runningNormKInput").value;
    const output = document.querySelector("#runningNormResult");
    const result = calcucateRunningNorm(m, k);
    result === null ? output.innerHTML = "Помилка введених данних, мають бути цілі числа" : output.innerHTML = result
};
import { handleFindMax } from './task_1.js';
import { handleRunningNorm } from './task_2.js';
import { handleDrawPythagorasTable } from './task_3.js';
import { handleFindRange } from './practice_task_1.js';
import { handleFindMax3 } from './practice_task_2.js';
import { handleGetOddFromRange } from './practice_task_3.js';
import { handleGetSeason } from './practice_task_4.js';

document.querySelector("#findMaxClick").addEventListener("click", handleFindMax);
document.querySelector("#runningNormClick").addEventListener("click", handleRunningNorm);
document.querySelector("#findRangeClick").addEventListener("click", handleFindRange);

document.querySelector("#pythagorasTableClick").addEventListener("click", handleDrawPythagorasTable);
document.querySelector("#findMax3Click").addEventListener("click", handleFindMax3);
document.querySelector("#getOddFromRangeClick").addEventListener("click", handleGetOddFromRange);
document.querySelector("#getSeasonClick").addEventListener("click", handleGetSeason);
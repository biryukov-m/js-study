const findRange = num => {
    if (Number(num) !== Math.ceil(num)) {
        return "Число має бути цілим";
    }
    let result = '';
    switch (Number(num)) {
        case 4:
        case 5:
        case 6:
        case 7:
            result = 'grey';
            break;
        case 8:
        case 9:
        case 10:
        case 11:
            result = 'grey, red';
            break;
        case 12:
            result = 'grey, red, blue';
            break;
        case 13:
        case 14:
        case 15:
        case 16:
        case 17:
        case 18:
        case 19:
        case 20:
        case 21:
        case 22:
        case 23:
        case 24:
        case 25:
            result = 'red, blue';
            break;
        case 26:
        case 27:
        case 28:
        case 29:
        case 30:
        case 31:
            result = 'blue';
            break;
        default:
            result = 'Число не входить до діапазону';
    }
    return result;
};

export const handleFindRange = () => {
    const input = document.querySelector("#findRangeInput").value;
    const output = document.querySelector("#findRangeResult");
    output.innerHTML = findRange(input);
}

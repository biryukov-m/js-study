//  Знайти більше з трьох чисел.Можна використовувати тільки один оператор if

const findMax3 = (first, second, third) => {
    if (first > second && first > third) {
        return first;

    } else if (second > first && second > third) {
        return second;
    }
    return third;
};

export const handleFindMax3 = () => {
    const input = [];
    const output = document.getElementById("findMax3Result");
    for (let i = 1; i <= 3; i++) {
        input.push(Number(document.getElementById(`findMax3Input${i}`).value));
    }
    const result = findMax3(input[0], input[1], input[2]);
    output.innerHTML = result;
};
function findOldest(users) {
    let maxAge = 0;
    let oldest = '';
    for (const user of users) {
        if (user.age > maxAge) {
            maxAge = user.age;
            oldest = user.name;
        }
    }
    return oldest;
}

export const handleFindOldest = () => {
    const firstName = document.querySelector("#firstUserNameInput").value;
    const firstAge = document.querySelector("#firstUserAge").value;
    const secondName = document.querySelector("#secondUserNameInput").value;
    const secondAge = document.querySelector("#secondUserAge").value;
    const thirdName = document.querySelector("#thirdUserNameInput").value;
    const thirdAge = document.querySelector("#thirdUserAge").value;
    const fourthName = document.querySelector("#fourthUserNameInput").value;
    const fourthAge = document.querySelector("#fourthUserAge").value;
    const fifthName = document.querySelector("#fifthUserNameInput").value;
    const fifthAge = document.querySelector("#fifthUserAge").value;

    const input = [
        { name: firstName, age: firstAge },
        { name: secondName, age: secondAge },
        { name: thirdName, age: thirdAge },
        { name: fourthName, age: fourthAge },
        { name: fifthName, age: fifthAge },
    ];

    const output = document.querySelector("#findOldestResult");
    output.innerHTML = findOldest(input);
}

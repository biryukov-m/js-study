const combinateHundreds = (num) => {
    if (num < 100 || num > 999) { return null }
    const first = Math.floor(num / 100) % 10;
    const second = Math.floor(num / 10) % 10;
    const third = num % 10;
    const result = [
        `${first}${second}${third}`,
        `${first}${third}${second}`,
        `${second}${first}${third}`,
        `${second}${third}${first}`,
        `${third}${second}${third}`,
        `${third}${third}${second}`
    ]
    let filteredResult = [];

    result.map(number => {
        if (!(filteredResult.includes(number))) {
            filteredResult.push(number);
        };
    });

    return filteredResult;
}

export const handleCombinateHundreds = () => {
    const input = document.querySelector("#combinateHundredsInput").value;
    const output = document.querySelector("#combinateHundredsResult");
    const result = combinateHundreds(input);

    output.innerHTML = result ? result : "Введіть трьохзначне число!";
};
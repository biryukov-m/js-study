const checkModThree = num => num % 3 > 0 ? num % 3 : true;

export const handleCheckModThree = () => {
    const input = document.querySelector("#checkModThreeInput").value;
    const output = document.querySelector("#checkModThreeResult");
    const result = checkModThree(input);
    output.innerHTML = result === true ? "Число кратне трьом" : `Число не кратне трьом, залишок - ${result}`;
};
const checkOdd = num => num % 2 == 0;

export const handleCheckOdd = () => {
    const input = document.querySelector("#checkOddInput").value;
    const output = document.querySelector("#checkOddResult");
    output.innerHTML = checkOdd(input) ? "Число парне" : "Число непарне";
}

const isLeapYear = year => year % 4 == 0 && !(year % 100 == 0) || year % 400 == 0;

export const handleCheckLeapYear = () => {
    const input = document.querySelector("#checkLeapYear").value;
    const output = document.querySelector("#checkLeapYearResult");
    output.innerHTML = isLeapYear(input) ? "Рік високосний" : "Рік не високосний";
}
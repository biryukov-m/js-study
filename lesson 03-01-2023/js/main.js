import { handleCheckOdd } from './task _1.js';
import { handleCheckModThree } from './task_2.js';
import { handleCombinateHundreds } from './task_3.js';
import { handleToKilobytes } from './practice_task_1.js';
import { handleFindOldest } from './practice_task_2.js';
import { handleCheckLeapYear } from './practice_task_3.js';


document.querySelector("#checkOddClick").addEventListener("click", handleCheckOdd);
document.querySelector("#checkModThreeClick").addEventListener("click", handleCheckModThree);
document.querySelector("#combinateHundredsClick").addEventListener("click", handleCombinateHundreds);
document.querySelector("#toKilobytesClick").addEventListener("click", handleToKilobytes);
document.querySelector("#findOldestClick").addEventListener("click", handleFindOldest);
document.querySelector("#checkLeapYearClick").addEventListener("click", handleCheckLeapYear);
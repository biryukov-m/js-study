const toKilobytes = bytes => (bytes / 1024).toFixed(2);

export const handleToKilobytes = () => {
    const input = document.querySelector("#toKilobytesInput").value;
    const output = document.querySelector("#toKilobytesResult");
    output.innerHTML = `${toKilobytes(input)} кб`;
}

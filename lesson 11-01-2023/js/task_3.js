// 3) Дано натуральні числа n і m.Написати функцію, яка
// повертає результат операції додавання двох чисел.Перше
// утворено з k молодших цифр числа n, друге - з k старших
// цифр числа m.

const sumDigitsRange = (n, m, k) => {
    if (10 ** k > m || 10 ** k > n) {
        return null;
    }
    while (m > 10 ** k) {
        m = Math.floor(m / 10);
    }
    let sum = 0;
    for (let i = 0; i < k; i++) {
        sum += n % 10;
        n = Math.floor(n / 10);
        sum += m % 10;
        m = Math.floor(m / 10);
    }
    return sum;
};


export const handleSumDigitsRange = () => {
    const n = +document.querySelector("#sumDigitsRangeN").value;
    const m = +document.querySelector("#sumDigitsRangeM").value;
    const k = +document.querySelector("#sumDigitsRangeK").value;
    const output = document.querySelector("#sumDigitsRangeResult");
    output.innerHTML = sumDigitsRange(n, m, k) != null ? sumDigitsRange(n, m, k) : "k має бути меншим, ніж кількість розрядів у числа m або k";
}
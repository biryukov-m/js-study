// 1) Необхідно реалізувати функцію, що друкує всі прості числа
// із заданого діапазону.

const isPrimal = num => {
    if (num < 2) return false;
    let i = 2;
    while (i < num) {
        if (num % i === 0) {
            return false;
        }
        i++;
    }
    return true;
};

const findPrimals = (start, end) => {
    let primals = [];
    for (let i = start; i <= end; i++) {
        isPrimal(i) && primals.push(i);
    }
    return primals;
};


export const handleFindPrimals = () => {
    const start = +document.querySelector("#findPrimalsStart").value;
    const end = +document.querySelector("#findPrimalsEnd").value;
    const output = document.querySelector("#findPrimalsResult");
    output.innerHTML = findPrimals(start, end);
}

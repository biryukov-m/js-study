// 2) Знайти суму цифр заданого числа.

const sumDigits = num => {
    let sum = 0;
    while (num / 10 > 0) {
        sum += num % 10;
        num = Math.floor(num / 10);
    }
    return sum;
}

export const handleSumDigits = () => {
    const num = +document.querySelector("#sumDigitsInput").value;
    const output = document.querySelector("#sumDigitsResult");
    output.innerHTML = sumDigits(num);
}

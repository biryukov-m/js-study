import { handleFindPrimals } from './task_1.js';
import { handleSumDigits } from './task_2.js';
import { handleSumDigitsRange } from './task_3.js';

document.querySelector("#findPrimalsClick").addEventListener("click", handleFindPrimals);
document.querySelector("#sumDigitsClick").addEventListener("click", handleSumDigits);
document.querySelector("#sumDigitsRangeClick").addEventListener("click", handleSumDigitsRange);
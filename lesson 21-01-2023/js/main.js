const items = [
    {
        id: 1,
        imageUrl: "./images/image-catalogue-page-1-fluid.png",
        imageAlt: "fluid for face",
        name: "Флюїд для обличчя для сухої шкіри",
        price: 630,
    },
    {
        id: 2,
        imageUrl: "./images/image-catalogue-page-2-antiox-cream.png",
        imageAlt: "antiox cream",
        name: "Антиоксидантний крем для обличчя",
        price: 850,
    },
    {
        id: 3,
        imageUrl: "./images/image-catalogue-page-3-shower-gel.png",
        imageAlt: "shower gel",
        name: "Гель для вмивання для проблемної шкіри",
        price: 590,
    },
    {
        id: 4,
        imageUrl: "./images/image-catalogue-page-4-moisturizing serum.png",
        imageAlt: "moisturizing serum",
        name: "Зволожуюча сироватка для нормальної шкіри",
        price: 490,
    },
    {
        id: 5,
        imageUrl: "./images/image-catalogue-page-5-moisturizing-cream.png",
        imageAlt: "moisturizing-cream",
        name: "Зволожуючий крем для сухої шкіри",
        price: 830,
    },
    {
        id: 6,
        imageUrl: "./images/image-catalogue-page-6-face-tonic.png",
        imageAlt: "face-tonic",
        name: "Тонік для обличчя для нормальної шкіри",
        price: 450,
    },
    {
        id: 7,
        imageUrl: "./images/image-catalogue-page-7-cleaning-foam.png",
        imageAlt: "cleaning-foam",
        name: "Пінка для очищення шкіри обличчя",
        price: 560,
    },
    {
        id: 8,
        imageUrl: "./images/image-catalogue-page-8-day-night-cream.png",
        imageAlt: "day night cream",
        name: "Набір Денний та нічний крем",
        price: 1230,
    },
    {
        id: 9,
        imageUrl: "./images/image-catalogue-page-9-mattifying-cream.png",
        imageAlt: "mattifying cream",
        name: "Матуючий денний крем для жирної шкіри",
        price: 680,
    },
    {
        id: 10,
        imageUrl: "./images/image-catalogue-page-10-night-anti-aging-cream.png",
        imageAlt: "night anti aging cream",
        name: "Нічний антивіковий крем для сухої шкіри",
        price: 860,
    },
    {
        id: 11,
        imageUrl: "./images/image-catalogue-page-11-make-up-remover-gel.png",
        imageAlt: "make-up-remover-gel",
        name: "Гель для зняття макіяжу",
        price: 730,
    },
    {
        id: 12,
        imageUrl: "./images/image-catalogue-page-12-anti-aging-serum.png",
        imageAlt: "anti-aging-serum",
        name: "Антивікова сироватка для зони навколо очей",
        price: 900,
    },
    {
        id: 13,
        imageUrl: "./images/image-catalogue-page-13-lotion-skin.png",
        imageAlt: "lotion-skin",
        name: "Лосьон для нормальної та комбінованої шкіри",
        price: 1050,
    },
    {
        id: 14,
        imageUrl: "./images/image-catalogue-page-14-scrub-sensitive.png",
        imageAlt: "scrub-sensitive",
        name: "Скраб для чутливої шкіри",
        price: 890,
    },
    {
        id: 15,
        imageUrl: "./images/image-catalogue-page-15-gel-deep-cleaning.png",
        imageAlt: "gel-deep-cleaning",
        name: "Гель для глубокого очищення шкіри обличчя",
        price: 890,
    },
];

const renderItems = () => {
    const root = document.querySelector("#catalogue_items");
    for (const item of items) {
        const divItem = document.createElement("div");
        divItem.classList.add("item");

        const divImageHolder = document.createElement("div");
        divImageHolder.classList.add("image-holder");
        const img = document.createElement("img");
        img.setAttribute("src", item.imageUrl);
        img.setAttribute("alt", item.imageAlt);
        const heart = document.createElement("span");
        heart.classList.add("heart");
        divImageHolder.append(img, heart);
        divItem.append(divImageHolder);

        const divName = document.createElement("div");
        divName.classList.add("name");
        const pName = document.createElement("p");
        pName.innerText = item.name;
        divName.append(pName);
        divItem.append(divName);

        const divPrice = document.createElement("div");
        divPrice.classList.add("price");
        const pPrice = document.createElement("p");
        pPrice.innerText = item.price + " грн";
        divPrice.append(pPrice);
        divItem.append(divPrice);

        const divControls = document.createElement("div");
        divControls.classList.add("controls");
        const aDetail = document.createElement("a");
        aDetail.setAttribute("href", "./product_card.html");
        aDetail.innerText = "Детальніше";
        divControls.append(aDetail);
        const divBtn = document.createElement("div");
        divBtn.classList.add("btn-mini");
        const spanToBasket = document.createElement("span");
        spanToBasket.innerText = "В кошик";
        divBtn.append(spanToBasket);
        divControls.append(divBtn);

        divItem.append(divControls);
        root.append(divItem);
    }
};

document.addEventListener("DOMContentLoaded", e => {
    renderItems();
});